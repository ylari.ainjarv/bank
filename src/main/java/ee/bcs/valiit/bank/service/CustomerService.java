package ee.bcs.valiit.bank.service;

import ee.bcs.valiit.bank.data.Customer;
import ee.bcs.valiit.bank.data.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public List<Customer> list() {
        return customerRepository.findAll();
    }

}
