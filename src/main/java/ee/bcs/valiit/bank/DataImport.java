package ee.bcs.valiit.bank;

import ee.bcs.valiit.bank.data.Account;
import ee.bcs.valiit.bank.data.AccountRepository;
import ee.bcs.valiit.bank.data.Customer;
import ee.bcs.valiit.bank.data.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Component
public class DataImport {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private AccountRepository accountRepository;

    private void start(String[] args) {
        try {
            String filePath = args[0];
            Path path = Paths.get(filePath);
            List<String> fileLines = Files.readAllLines(path);
            for (String fileLine : fileLines) {
                String[] lineParts = fileLine.split(", ");
                Customer customer = new Customer();
                customer.setFirstname(lineParts[0]);
                customer.setSurname(lineParts[1]);
                customer = customerRepository.save(customer);
                Account account = new Account();
                account.setAccountNumber(Long.parseLong(lineParts[2]));
                account.setBalance(new BigDecimal(lineParts[3]));
                account.setCustomerId(customer.getId());
                account.setActive(true);
                accountRepository.save(account);
            }
        } catch (IOException e) {
            System.err.println("Importfaili lugemine ebaõnnestus!");
        }
    }

    public static void main(String[] args) {
        if (args.length > 0) {
            ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
            DataImport dataImport = applicationContext.getBean(DataImport.class);
            dataImport.start(args);
        } else {
            System.out.println("Importfaili asukoht on kohustuslik!");
        }
    }

}
