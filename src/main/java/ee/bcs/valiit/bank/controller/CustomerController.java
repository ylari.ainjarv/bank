package ee.bcs.valiit.bank.controller;

import ee.bcs.valiit.bank.data.Customer;
import ee.bcs.valiit.bank.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/customer")
@Slf4j
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @GetMapping(value = "/list", produces = "application/json")
    public List<Customer> list() {
        log.debug("Customer list called ...");
        return customerService.list();
    }

}
